package gorepl

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

const internalCommandPrefix = "."

type internalCommand func(string) (string, error)

var internalCommands = map[string]internalCommand{
	"quit": func(string) (string, error) {
		fmt.Println("Quitting")
		os.Exit(0)
		return "", errors.New("quitting")
	},
}

func runInternalCmd(raw string) (string, error) {
	// Split name from string after removing annoying prefix
	splits := strings.SplitN(raw[len(internalCommandPrefix):], " ", 1)
	cmd, cmdExists := internalCommands[splits[0]]
	if !cmdExists {
		return "No such command", ErrNoSuchCommand
	}
	args := ""
	if len(splits) == 2 {
		args = splits[1]
	}
	return cmd(args)
}
