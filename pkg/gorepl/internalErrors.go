package gorepl

import "errors"

var ErrNoSuchCommand = errors.New("repl: No such internal command exists")
