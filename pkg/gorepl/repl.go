package gorepl

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Repl launches a Read Evaluate Print loop where the runner is used for evaluation
func Repl(runner func(string) (string, error)) error {
	breakout := false
	var breakErr error
	reader := bufio.NewReader(os.Stdin)
	for !breakout {
		input, err := reader.ReadString('\n')
		if err != nil {
			breakout = true
			breakErr = err
		}
		// Skip empty input
		if len(input) == 0 {
			continue
		}
		// Strip input of annoying stuff at start and end
		input = strings.Trim(input, " \r\n")

		// Check for internal command
		if strings.HasPrefix(input, internalCommandPrefix) {
			out, err := runInternalCmd(input)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(out)
			}
		} else {
			out, err := runner(input)
			if err != nil {
				breakout = true
				breakErr = err
				continue
			}
			fmt.Println(out)
		}
	}
	return breakErr
}
